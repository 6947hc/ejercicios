import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Formulario1Component } from './component/formulario1/formulario1.component';
import { Formulario2Component } from './component/formulario2/formulario2.component';
import { TablaComponent } from './component/tabla/tabla.component';
import { Formulario3Component } from './component/formulario3/formulario3.component';

@NgModule({
  declarations: [
    AppComponent,
    Formulario1Component,
    Formulario2Component,
    TablaComponent,
    Formulario3Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
