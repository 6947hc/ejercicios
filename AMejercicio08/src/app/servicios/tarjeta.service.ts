import { Injectable } from '@angular/core';
import { DatosUsuario } from '../interfaces/tarjeta.interface';
@Injectable({
  providedIn: 'root'
})
export class TarjetaService {
   listUsuarios: DatosUsuario[] = [
    {titular:'NEMO', numeroTarjeta: '1234567891234567', fechaExpiracion: '16/22'},
    {titular:'NEMO', numeroTarjeta: '1234567891004567', fechaExpiracion: '16/29'},
    {titular:'NEMO', numeroTarjeta: '1234567891234567', fechaExpiracion: '16/22'},

  ];
  constructor() { }

  getUsuario(){
    return this.listUsuarios.slice()
  }

  eliminarUsuario(index:number){
    this.listUsuarios.splice(index,1);
  }
}
